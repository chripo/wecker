package org.bitreich.wecker.receiver;

import org.bitreich.wecker.service.WSHelper;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class ShutdownReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        WSHelper.removeNotification(context);
    }
}
