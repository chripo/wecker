package org.bitreich.wecker.receiver;

import org.bitreich.wecker.WeckerAction;
import org.bitreich.wecker.service.WSHelper;
import org.bitreich.wecker.service.WeckerService.FLAG;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class NotificationReceiver extends BroadcastReceiver {

    protected final static String EXTRA_FLAG = "EXTRA_FLAG";

    public static void triggerUpdate(final Context ctx, final FLAG flag) {
        final Intent i = new Intent(ctx, NotificationReceiver.class);
        i.putExtra(EXTRA_FLAG, flag.ordinal());
        ctx.sendBroadcast(i);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        final FLAG flag = FLAG.values()[intent.getIntExtra(EXTRA_FLAG, FLAG.NONE.ordinal())];

        context.startService(WSHelper.newIntent(context, WeckerAction.NOTIFICATION, flag));
    }
}
