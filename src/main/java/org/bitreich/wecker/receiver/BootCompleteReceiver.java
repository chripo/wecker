package org.bitreich.wecker.receiver;

import org.bitreich.wecker.WeckerAction;
import org.bitreich.wecker.service.WSHelper;
import org.bitreich.wecker.service.WeckerService.FLAG;
import org.bitreich.wecker.util.ContextHelper;
import org.bitreich.wecker.util.Time;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class BootCompleteReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        // check for our package
        if (intent.getAction().equals(Intent.ACTION_PACKAGE_REPLACED))
              if (!intent.getData().getSchemeSpecificPart().equals(context.getPackageName()))
                  return;

        final AlarmManager am = ContextHelper.getAlarmManager(context);
        final Intent i = WSHelper.newIntent(context, WeckerAction.RESTORE, FLAG.NONE);

        final PendingIntent pi = PendingIntent.getService(context, 666666, i, PendingIntent.FLAG_CANCEL_CURRENT);
        am.cancel(pi);
        am.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + Time.Interval.SECOND.X(30), pi);
    }
}
