package org.bitreich.wecker;

import org.bitreich.wecker.data.DataDescriptor.Alarm;
import org.bitreich.wecker.data.DataDescriptor.WeckerType;
import org.bitreich.wecker.service.WSHelper;
import org.bitreich.wecker.util.Time;
import org.bitreich.wecker.view.main.AlarmListingFragment;
import org.bitreich.wecker.view.prefs.WeckerPreferences;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends AbsWeckerActivity {

    protected ViewPager mPager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.main);

        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(new ViewAdapter(getSupportFragmentManager()));

        final Cursor c = WSHelper.getEnabledWeckers(this, Time.now(), 1);
        try {
            if (c.getCount() > 0 && c.moveToFirst()
                    && c.getInt(c.getColumnIndex(Alarm.Cols.TYPE)) == WeckerType.TIMER.ordinal())
                        mPager.setCurrentItem(1, true);
            c.close();
        } catch (Exception ex) {}
    }

    public static class ViewAdapter extends FragmentPagerAdapter {

        public ViewAdapter(final FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int pos) {
            switch (pos) {
            case 0:
                return AlarmListingFragment.newInstance(WeckerType.ALARM);
            case 1:
                return AlarmListingFragment.newInstance(WeckerType.TIMER);
            default:
                return null;
            }
        }

        @Override
        public int getCount() {
            return 2;
        }
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        getWindow().setFormat(PixelFormat.RGBA_8888);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.item, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_item_prefs) {
            startActivity(new Intent(this, WeckerPreferences.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
