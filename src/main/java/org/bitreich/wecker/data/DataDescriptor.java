package org.bitreich.wecker.data;

import org.bitreich.wecker.WeckerAction;

import android.net.Uri;
import android.provider.BaseColumns;

public class DataDescriptor {

    public static final String CONTENT_AUTHORITY = WeckerAction.NS;
    protected static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    static public enum WeckerType {
        ALARM,
        TIMER
    }

    static public enum WeckerState {
        DISABLED,
        ENABLED
    }

    final public static class Alarm  {
        private Alarm() {}

        public static final String NAME = "alarm";
        public static final Uri CONTENT_URI =  BASE_CONTENT_URI.buildUpon().appendPath(NAME).build();
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + "." + NAME;
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/"+ CONTENT_AUTHORITY + "." + NAME;

        public static Uri buildUri(final int time) {
            return BASE_CONTENT_URI.buildUpon().appendPath(NAME).appendPath(""+time).build();
        }

        public interface Cols extends BaseColumns  {
            String TIME        = NAME + "_time";
            String TYPE        = NAME + "_type";
            String STATE       = NAME + "_state";
            String DUE         = NAME + "_due";
        }
    }
}
