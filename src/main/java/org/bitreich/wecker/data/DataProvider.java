package org.bitreich.wecker.data;

import java.util.ArrayList;

import org.bitreich.wecker.data.DataDescriptor.Alarm;
import org.bitreich.wecker.data.Database.Tables;

import android.content.ContentProvider;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentValues;
import android.content.OperationApplicationException;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

public class DataProvider extends ContentProvider {

    private Database mDBHelper;

    interface TOK {
        int ALARM = 100;
        int ALARM_ID = ALARM + 1;
    }

    public static final UriMatcher URI_MATCHER = buildUriMatcher();

    private static UriMatcher buildUriMatcher() {
        final String CA = DataDescriptor.CONTENT_AUTHORITY;
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);

        matcher.addURI(CA, Alarm.NAME, TOK.ALARM);
        matcher.addURI(CA, Alarm.NAME + "/#", TOK.ALARM_ID);

        return matcher;
    }

    @Override
    public String getType(Uri uri) {
        switch ( URI_MATCHER.match(uri) ) {
        case TOK.ALARM:
            return Alarm.CONTENT_TYPE;

        case TOK.ALARM_ID:
            return Alarm.CONTENT_ITEM_TYPE;

        default:
            throw new UnsupportedOperationException("Unknown URI " + uri);
        }
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {

        final SQLiteDatabase db = mDBHelper.getReadableDatabase();
        switch ( URI_MATCHER.match(uri) ) {
        case TOK.ALARM: {
            final SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
            builder.setTables(Tables.ALARM);
            return builder.query(db, projection, selection, selectionArgs, null, null, sortOrder);
            }

        default:
            return null;
        }
    }

    @Override
    public boolean onCreate() {
        mDBHelper = new Database(getContext());

        return true;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase db = mDBHelper.getWritableDatabase();
        final int count;
        switch ( URI_MATCHER.match(uri) ) {
            case TOK.ALARM : {
                count = db.delete(Tables.ALARM, selection, selectionArgs);
                getContext().getContentResolver().notifyChange(uri, null);
                break;
            }
            default: {
                throw new UnsupportedOperationException("URI: " + uri + " not supported.");
            }
        }
        return count;
    }

    @Override
    public Uri insert(final Uri uri, final ContentValues values) {

        final SQLiteDatabase db = mDBHelper.getWritableDatabase();
        final Uri result;
        switch (URI_MATCHER.match(uri)) {
            case TOK.ALARM : {
                final long id = db.insert(Tables.ALARM, null, values);
                getContext().getContentResolver().notifyChange(uri, null);
                result = id >= 0 ? Alarm.buildUri(values.getAsInteger(Alarm.Cols.TIME)) : null;
                break;
            }
            default: {
                throw new UnsupportedOperationException("URI: " + uri + " not supported.");
            }
        }

        return result;
    }

    @Override
    public ContentProviderResult[] applyBatch(ArrayList<ContentProviderOperation> operations)
            throws OperationApplicationException {

        final SQLiteDatabase db = mDBHelper.getWritableDatabase();
        db.beginTransaction();
        try {
            final int numOperations = operations.size();
            final ContentProviderResult[] results = new ContentProviderResult[numOperations];
            for (int i = 0; i < numOperations; i++)
                results[i] = operations.get(i).apply(this, results, i);
            db.setTransactionSuccessful();
            return results;
        } finally {
            db.endTransaction();
        }
    }

    @Override
    public int update(final Uri uri, final ContentValues values, final String selection, final String[] selectionArgs) {
        SQLiteDatabase db = mDBHelper.getWritableDatabase();
        final int count;
        switch (URI_MATCHER.match(uri)) {
            case TOK.ALARM : {
                count = db.update(Tables.ALARM, values, selection, selectionArgs);
                getContext().getContentResolver().notifyChange(uri, null);
                break;
            }
            default: {
                throw new UnsupportedOperationException("URI: " + uri + " not supported.");
            }
        }
        return count;
    }
}
