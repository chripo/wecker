package org.bitreich.wecker.data;

import org.bitreich.wecker.data.DataDescriptor.Alarm;
import org.bitreich.wecker.data.DataDescriptor.WeckerState;
import org.bitreich.wecker.data.DataDescriptor.WeckerType;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Database extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;

    public Database(Context ctx) {
        super(ctx, "wecker.sqlite", null, DATABASE_VERSION);
    }

    public static interface Tables {
        String ALARM = Alarm.NAME;
    }

    public static interface Qualified {
        String ALARM_ID = Tables.ALARM + "." + Alarm.Cols._ID;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS " + Tables.ALARM + "("
            + Alarm.Cols._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + Alarm.Cols.TIME + " UNSIGNED INTEGER (0, 86399),"
            + Alarm.Cols.TYPE + " UNSIGNED INTEGER (0," + (WeckerType.values().length - 1) + ") DEFAULT " + WeckerType.ALARM.ordinal() + ","
            + Alarm.Cols.STATE + " UNSIGNED INTEGER (0,1) DEFAULT " + WeckerState.DISABLED.ordinal() + ","
            + Alarm.Cols.DUE + " UNSIGNED INTEGER DEFAULT 0,"
            + "UNIQUE (" + Alarm.Cols.TIME + "," + Alarm.Cols.TYPE + ") ON CONFLICT IGNORE"
        + ");");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + Tables.ALARM);
        onCreate(db);
    }
}
