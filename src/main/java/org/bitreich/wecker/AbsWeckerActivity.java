package org.bitreich.wecker;

import org.bitreich.wecker.util.Time;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;

abstract public class AbsWeckerActivity extends FragmentActivity {

	private BroadcastReceiver mVolumeReceiver;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		Time.init(this);

		super.onCreate(savedInstanceState);

		setVolumeControlStream(AudioManager.STREAM_ALARM);
		mVolumeReceiver = new BroadcastReceiver() {

			Ringtone mTone;

			@Override
			public void onReceive(final Context context, final Intent intent) {
				try {
					if ( mTone == null ) {
						final Uri tone = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
						mTone = RingtoneManager.getRingtone(getApplicationContext(), tone);
						mTone.setStreamType(AudioManager.STREAM_ALARM);
					} else {
						if (mTone.isPlaying())
							return;
						mTone.stop();
					}

					mTone.play();

					final Handler handler = new Handler();
					handler.postDelayed(new Runnable() {
						@Override
						public void run() {
							try { mTone.stop(); } catch (Exception ex) {};
						}
					}, 1000);
				} catch (Exception ex) {}
			}
		};
	}

	@Override
	protected void onResume() {
		super.onResume();

		registerReceiver(mVolumeReceiver,
				new IntentFilter("android.media.VOLUME_CHANGED_ACTION"));
	}

	@Override
	protected void onPause() {
		super.onPause();
		unregisterReceiver(mVolumeReceiver);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
}
