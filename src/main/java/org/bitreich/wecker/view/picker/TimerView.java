package org.bitreich.wecker.view.picker;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.bitreich.wecker.R;

public class TimerView extends LinearLayout {

	protected TextView mHoursTens, mHoursOnes,
			mMinutesTens, mMinutesOnes, mHoursSeperator;

	public TimerView(final Context context) {
		this(context, null);
	}

	public TimerView(final Context context, final AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();

		mHoursTens = (TextView) findViewById(R.id.hours_tens);
		mHoursOnes = (TextView) findViewById(R.id.hours_ones);
		mHoursSeperator = (TextView) findViewById(R.id.hours_seperator);
        mMinutesTens = (TextView) findViewById(R.id.minutes_tens);
        mMinutesOnes = (TextView) findViewById(R.id.minutes_ones);

		if (!isInEditMode()) {
			final Typeface font = Typeface.createFromAsset(getContext().getAssets(), "fonts/roboto-thin.ttf");
			mHoursTens.setTypeface(font);
			mHoursOnes.setTypeface(font);
			mHoursSeperator.setTypeface(font);
			mMinutesTens.setTypeface(font);
			mMinutesOnes.setTypeface(font);
		}
	}

	public void setTime(final int h, final int m) {
		setTime(h / 10, h % 10, m / 10, m % 10);
	}

	public void setTime(final int hoursTensDigit, final int hoursOnesDigit,
						final int minutesTensDigit, final int minutesOnesDigit) {
		// Hide digit
		if (hoursTensDigit == -2) {
			mHoursTens.setVisibility(View.INVISIBLE);
		} else if (hoursTensDigit == -1) {
			mHoursTens.setText("-");
			mHoursTens.setEnabled(false);
			mHoursTens.setVisibility(View.VISIBLE);
		} else {
			mHoursTens.setText(String.format("%d", hoursTensDigit));
			mHoursTens.setEnabled(true);
			mHoursTens.setVisibility(View.VISIBLE);
		}

		if (hoursOnesDigit == -2) {
			mHoursOnes.setVisibility(View.INVISIBLE);
		} else if (hoursOnesDigit == -1) {
			mHoursOnes.setText("-");
			mHoursOnes.setEnabled(false);
			mHoursOnes.setVisibility(View.VISIBLE);
		} else {
			mHoursOnes.setText(String.format("%d", hoursOnesDigit));
			mHoursOnes.setEnabled(true);
			mHoursOnes.setVisibility(View.VISIBLE);
		}

		if (hoursTensDigit == -2 && hoursOnesDigit == -2)
			mHoursSeperator.setVisibility(View.INVISIBLE);
		else
			mHoursSeperator.setVisibility(View.VISIBLE);

		if (minutesTensDigit == -1) {
			mMinutesTens.setText("-");
			mMinutesTens.setEnabled(false);
		} else {
			mMinutesTens.setEnabled(true);
			mMinutesTens.setText(String.format("%d", minutesTensDigit));
		}

		if (minutesOnesDigit == -1) {
			mMinutesOnes.setText("-");
			mMinutesOnes.setEnabled(false);
		} else {
			mMinutesOnes.setText(String.format("%d", minutesOnesDigit));
			mMinutesOnes.setEnabled(true);
		}
    }
}
