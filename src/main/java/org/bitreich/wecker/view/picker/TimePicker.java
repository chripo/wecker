package org.bitreich.wecker.view.picker;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import org.bitreich.wecker.R;
import org.bitreich.wecker.data.DataDescriptor.WeckerType;

public class TimePicker extends LinearLayout implements Button.OnClickListener, Button.OnLongClickListener {

	protected final Button mNums[] = new Button[10];

	protected int mInputSize = 4;
	protected int mInput[] = new int[mInputSize];
	protected int mInputPointer = -1;

	protected TimerView mTime;
	protected ImageButton mDelete;
	protected View mSetButton;
	protected WeckerType mType = WeckerType.ALARM;

	public TimePicker(final Context context) {
		this(context, null);
		try {
			onFinishInflate();
		} catch (final Exception e) {}
	}

	public TimePicker(final Context context, final AttributeSet attrs) {
		super(context, attrs);

		LayoutInflater.from(context).inflate(R.layout.timerpicker_view, this);
	}

	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();

		final View v1 = findViewById(R.id.first);
		final View v2 = findViewById(R.id.second);
		final View v3 = findViewById(R.id.third);
		final View v4 = findViewById(R.id.fourth);

		mTime = (TimerView) findViewById(R.id.timer_time_text);
		mDelete = (ImageButton) findViewById(R.id.delete);
		mDelete.setOnClickListener(this);
		mDelete.setOnLongClickListener(this);

		mNums[0] = (Button) v4.findViewById(R.id.key_middle);

		mNums[1] = (Button) v1.findViewById(R.id.key_left);
		mNums[2] = (Button) v1.findViewById(R.id.key_middle);
		mNums[3] = (Button) v1.findViewById(R.id.key_right);

		mNums[4] = (Button) v2.findViewById(R.id.key_left);
		mNums[5] = (Button) v2.findViewById(R.id.key_middle);
		mNums[6] = (Button) v2.findViewById(R.id.key_right);

		mNums[7] = (Button) v3.findViewById(R.id.key_left);
		mNums[8] = (Button) v3.findViewById(R.id.key_middle);
		mNums[9] = (Button) v3.findViewById(R.id.key_right);

		v4.findViewById(R.id.key_left).setVisibility(View.INVISIBLE);
		v4.findViewById(R.id.key_right).setVisibility(View.INVISIBLE);

		final Typeface font = !isInEditMode() ? Typeface.createFromAsset(getContext().getAssets(), "fonts/roboto-thin.ttf") : null;
		 for (int i = 0; i < mNums.length; ++i) {
			if (font != null)
				mNums[i].setTypeface(font);
			mNums[i].setOnClickListener(this);
			mNums[i].setText(String.format("%d", i));
			mNums[i].setTag(R.id.numbers_key, new Integer(i));
		}

		updateView();
	}

	@Override
	public void onClick(final View v) {
		v.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
		onKeyClick(v);
	}

	protected void updateDeleteButton() {
		mDelete.setEnabled(mInputPointer != -1);
	}

	protected void onKeyClick(final View view) {
		final Integer num = (Integer) view.getTag(R.id.numbers_key);
		if (num != null) {
			addNum(num);
		} else if (view == mDelete) {
			for (int i = 0; i < mInputPointer; i++)
				mInput[i] = mInput[i + 1];
			mInput[mInputPointer] = 0;
			mInputPointer--;
		}

		updateView();
	}

	protected void updateView() {
		updateTime();
		updateKeys();
		updateSetButton();
		updateDeleteButton();
	}

	protected void addNum(final Integer num) {
		if (mInputPointer < mInputSize - 1) {
			for (int i = mInputPointer; i >= 0; --i)
				mInput[i + 1] = mInput[i];
			mInputPointer++;
			mInput[0] = num;
		}
	}

	protected void updateKeys() {
		final int time = getTime();
		if (mInputPointer >= 3 || mInputPointer == 2 && mInput[0] > 5) {
			setKeyRange(-1);
		} else if (time == 0) {
			if (mInputPointer == -1 || mInputPointer == 0 || mInputPointer == 2) {
				setKeyRange(9);
			} else if (mInputPointer == 1) {
				setKeyRange(5);
			} else {
				setKeyRange(-1);
			}
		} else if (time == 1) {
			if (mInputPointer == 0 || mInputPointer == 2) {
				setKeyRange(9);
			} else if (mInputPointer == 1) {
				setKeyRange(5);
			} else {
				setKeyRange(-1);
			}
		} else if (time == 2) {
			if (mInputPointer == 2 || mInputPointer == 1) {
				setKeyRange(9);
			} else if (mInputPointer == 0) {
				setKeyRange(5);
			} else {
				setKeyRange(-1);
			}
		} else if (time <= 5) {
			setKeyRange(9);
		} else if (time <= 9) {
			setKeyRange(5);
		} else if (time >= 10 && time <= 15) {
			setKeyRange(9);
		} else if (time >= 16 && time <= 19) {
			setKeyRange(5);
		} else if (time >= 20 && time <= 25) {
			setKeyRange(9);
		} else if (time >= 26 && time <= 29) {
			setKeyRange(-1);
		} else if (time >= 30 && time <= 35) {
			setKeyRange(9);
		} else if (time >= 36 && time <= 39) {
			setKeyRange(-1);
		} else if (time >= 40 && time <= 45) {
			setKeyRange(9);
		} else if (time >= 46 && time <= 49) {
			setKeyRange(-1);
		} else if (time >= 50 && time <= 55) {
			setKeyRange(9);
		} else if (time >= 56 && time <= 59) {
			setKeyRange(-1);
		} else if (time >= 60 && time <= 65) {
			setKeyRange(9);
		} else if (time >= 70 && time <= 75) {
			setKeyRange(9);
		} else if (time >= 80 && time <= 85) {
			setKeyRange(9);
		} else if (time >= 90 && time <= 95) {
			setKeyRange(9);
		} else if (time >= 100 && time <= 105) {
			setKeyRange(9);
		} else if (time >= 106 && time <= 109) {
			setKeyRange(-1);
		} else if (time >= 110 && time <= 115) {
			setKeyRange(9);
		} else if (time >= 116 && time <= 119) {
			setKeyRange(-1);
		} else if (time >= 120 && time <= 125) {
			setKeyRange(9);
		} else if (time >= 126 && time <= 129) {
			setKeyRange(-1);
		} else if (time >= 130 && time <= 135) {
			setKeyRange(9);
		} else if (time >= 136 && time <= 139) {
			setKeyRange(-1);
		} else if (time >= 140 && time <= 145) {
			setKeyRange(9);
		} else if (time >= 146 && time <= 149) {
			setKeyRange(-1);
		} else if (time >= 150 && time <= 155) {
			setKeyRange(9);
		} else if (time >= 156 && time <= 159) {
			setKeyRange(-1);
		} else if (time >= 160 && time <= 165) {
			setKeyRange(9);
		} else if (time >= 166 && time <= 169) {
			setKeyRange(-1);
		} else if (time >= 170 && time <= 175) {
			setKeyRange(9);
		} else if (time >= 176 && time <= 179) {
			setKeyRange(-1);
		} else if (time >= 180 && time <= 185) {
			setKeyRange(9);
		} else if (time >= 186 && time <= 189) {
			setKeyRange(-1);
		} else if (time >= 190 && time <= 195) {
			setKeyRange(9);
		} else if (time >= 196 && time <= 199) {
			setKeyRange(-1);
		} else if (time >= 200 && time <= 205) {
			setKeyRange(9);
		} else if (time >= 206 && time <= 209) {
			setKeyRange(-1);
		} else if (time >= 210 && time <= 215) {
			setKeyRange(9);
		} else if (time >= 216 && time <= 219) {
			setKeyRange(-1);
		} else if (time >= 220 && time <= 225) {
			setKeyRange(9);
		} else if (time >= 226 && time <= 229) {
			setKeyRange(-1);
		} else if (time >= 230 && time <= 235) {
			setKeyRange(9);
		} else if (time >= 236) {
			setKeyRange(-1);
		}
	}

	protected void setKeyRange(final int maxKey) {
		for (int i = 0; i < mNums.length; i++)
			mNums[i].setEnabled(i <= maxKey);
	}

	@Override
	public boolean onLongClick(final View v) {
		v.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS);
		if (v == mDelete) {
			mDelete.setPressed(false);
			reset();
			return  true;
		}
		return false;
	}

	public void reset() {
		for (int i = 0; i < mInputSize; i++)
			mInput[i] = 0;
		mInputPointer = -1;
		updateView();
	}

	protected void updateTime() {
		int hours1 = -1;
		// If the user entered 3 to 9 or 24 to 25 , there is no need for a 4th digit (24 hours mode)
		if (mInputPointer > -1) {
			// Test to see if the highest digit is 2 to 9 for AM/PM or 3 to 9 for 24 hours mode
			if (mInputPointer >= 0) {
				final int digit = mInput[mInputPointer];
				if (digit >= 3 && digit <= 9)
					hours1 = -2;
			}
			// Test to see if the 2 highest digits are 13 to 15 for AM/PM or 24 to 25 for 24 hours
			// mode
			if (mInputPointer > 0 && mInputPointer < 3 && hours1 != -2) {
				final int digits = mInput[mInputPointer] * 10 + mInput[mInputPointer - 1];
				if ((digits >= 24 && digits <= 25) || mInputPointer == 2 && mInput[0] > 5)
					hours1 = -2;
			}
			// If we have a digit show it
			if (mInputPointer == 3)
				hours1 = mInput[3];
		}

		final int hours2 = (mInputPointer < 2) ? -1 : mInput[2];
		final int minutes1 = (mInputPointer < 1) ? -1 : mInput[1];
		final int minutes2 = (mInputPointer < 0) ? -1 : mInput[0];
		mTime.setTime(hours1, hours2, minutes1, minutes2);
	}

	public int getCurrentHour() {
		return mInput[3] * 10 + mInput[2];
	}

	public void setCurrentHour(int hour) {
		mInput[3] = hour / 10;
		mInput[2] = hour % 10;

		mInputPointer = 3;

		updateView();
	}

	public int getCurrentMinute() {
		return mInput[1] * 10 + mInput[0];
	}

	public void setCurrentMinute(int minute) {
		mInput[1] = minute / 10;
		mInput[0] = minute % 10;

		if (mInputPointer < 2) {
			if (mInput[0] > 0)
				mInputPointer = 0;
			if (mInput[1] > 1)
				mInputPointer = 1;
		}

		updateView();
	}

	public int getTime() {
		return mInput[3] * 600 + mInput[2] * 60 + mInput[1] * 10 + mInput[0];
	}

	public void setSetButton(final View setButton) {
		mSetButton = setButton;
		updateSetButton();
	}

	public void setType(final WeckerType type) {
		mType = type;
	}

	protected void updateSetButton() {
		if (mSetButton == null)
			return;

		mSetButton.setEnabled(
			(mType == WeckerType.ALARM ? mInputPointer >= 2 : mInputPointer >= 0)
			&& getCurrentHour() < 25 && getCurrentMinute() < 60);
	}

	@Override
	protected Parcelable onSaveInstanceState() {
		final SavedState s = new SavedState(super.onSaveInstanceState());
		s.mInput = mInput;
		s.mInputPointer = mInputPointer;

		return s;
	}

	@Override
	protected void onRestoreInstanceState(Parcelable state) {
		if (!(state instanceof SavedState)) {
			super.onRestoreInstanceState(state);
			return;
		}
		final SavedState s = (SavedState) state;
		super.onRestoreInstanceState(s.getSuperState());

		if (s.mInput != null) {
			mInput = s.mInput;
			mInputPointer = s.mInputPointer;
		}

		updateView();
	}

	private static class SavedState extends BaseSavedState {

		public int mInputPointer;
		public int[] mInput;

		public SavedState(Parcelable source) {
			super(source);
		}

		public SavedState(Parcel in) {
			super(in);
			mInputPointer = in.readInt();
			in.readIntArray(mInput);
		}

		@Override
		public void writeToParcel(Parcel dest, int flags) {
			super.writeToParcel(dest, flags);
			dest.writeInt(mInputPointer);
			dest.writeIntArray(mInput);
		}
	}
}
