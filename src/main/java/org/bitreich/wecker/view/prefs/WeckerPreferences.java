package org.bitreich.wecker.view.prefs;

import org.bitreich.wecker.R;
import org.bitreich.wecker.WeckerAction;
import org.bitreich.wecker.service.WSHelper;
import org.bitreich.wecker.service.WeckerService.FLAG;
import org.bitreich.wecker.util.Prefs;
import org.bitreich.wecker.util.Prefs.KEY;

import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.util.Log;

@SuppressWarnings("deprecation")
public class WeckerPreferences extends PreferenceActivity implements OnSharedPreferenceChangeListener {

    protected boolean mIsRestModeChanged = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);

        setSummary(KEY.AUTOSETUP, Prefs.getAutosetup(getApplicationContext()).value,
                        R.array.pref_autosetup_entries, R.array.pref_autosetup_values);

        setSummary(KEY.TIMEOUT_SNOOZE,
                        "" + Prefs.getTimeout(getApplicationContext(), KEY.TIMEOUT_SNOOZE),
                        R.array.pref_time_entries, R.array.pref_time_values);

        setSummary(KEY.TIMEOUT_DISMISS,
                        "" + Prefs.getTimeout(getApplicationContext(), KEY.TIMEOUT_DISMISS),
                        R.array.pref_time_entries, R.array.pref_time_values);

        setSummary(KEY.QUIET_MODE, Prefs.getQuietMode(getApplicationContext()).value,
                        R.array.pref_quiet_mode_entries, R.array.pref_quiet_mode_values);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
        if (key.equals(Prefs.KEY.QUIET_START)) {
            mIsRestModeChanged = true;
        } else if (key.equals(Prefs.KEY.QUIET_MODE)) {
            mIsRestModeChanged = true;
            setSummary(KEY.QUIET_MODE, prefs.getString(key, null),
                    R.array.pref_quiet_mode_entries, R.array.pref_quiet_mode_values);
        } else if (key.equals(KEY.AUTOSETUP)) {
            setSummary(KEY.AUTOSETUP, prefs.getString(key, null),
                    R.array.pref_autosetup_entries, R.array.pref_autosetup_values);
        } else if (key.equals(KEY.TIMEOUT_SNOOZE)) {
            setSummary(KEY.TIMEOUT_SNOOZE,
                    prefs.getString(key, null),
                    R.array.pref_time_entries, R.array.pref_time_values);
        } else if (key.equals(KEY.TIMEOUT_DISMISS)) {
            setSummary(KEY.TIMEOUT_DISMISS,
                    prefs.getString(key, null),
                    R.array.pref_time_entries, R.array.pref_time_values);
        }
    }

    private void setSummary(final String key, final String valueKey, final int entArrKey, final int valArrKey) {
        try {
            final String[] vals = getResources().getStringArray(valArrKey);
            int idx = -1;
            for (final String v : vals) {
                ++idx;
                if (valueKey.equals(v))
                    break;
            }
            setSummary(key, getResources().getStringArray(entArrKey)[idx]);
        } catch (Exception e) {
            Log.e(WeckerAction.NS, "setSummary error: " + e.getMessage());
        }
    }

    private void setSummary(final String key, final String summary) {
        try {
            findPreference(key).setSummary(summary);
        } catch (Exception e) {}
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (mIsRestModeChanged)
            startService(WSHelper.newIntent(this, WeckerAction.QUIETMODE, FLAG.NONE));
    }

    @Override
    protected void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences()
                .registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences()
                .unregisterOnSharedPreferenceChangeListener(this);
    }
}
