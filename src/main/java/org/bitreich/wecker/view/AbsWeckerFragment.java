package org.bitreich.wecker.view;

import android.os.Bundle;
import android.support.v4.app.Fragment;

abstract public class AbsWeckerFragment extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);
    }
}
