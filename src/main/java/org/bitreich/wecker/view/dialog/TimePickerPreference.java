package org.bitreich.wecker.view.dialog;

import android.content.Context;
import android.content.res.TypedArray;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.view.View;

import org.bitreich.wecker.util.Time;
import org.bitreich.wecker.view.picker.TimePicker;

public class TimePickerPreference extends DialogPreference {
    private TimePicker picker = null;
    private int hour = 0;
    private int min = 0;

    public TimePickerPreference(Context ctxt) {
        this(ctxt, null);
    }

    public TimePickerPreference(Context ctxt, AttributeSet attrs) {
        this(ctxt, attrs, 0);
    }

    public TimePickerPreference(Context ctxt, AttributeSet attrs, int defStyle) {
        super(ctxt, attrs, defStyle);

		setPositiveButtonText(android.R.string.ok);
		setNegativeButtonText(android.R.string.cancel);
	}

    @Override
    protected View onCreateDialogView() {
		picker = new TimePicker(getContext());
        return picker;
    }

    @Override
    protected void onBindDialogView(View v) {
        super.onBindDialogView(v);
        picker.setCurrentHour(hour);
        picker.setCurrentMinute(min);
    }

    @Override
    protected void onDialogClosed(boolean positiveResult) {
        super.onDialogClosed(positiveResult);

        if (positiveResult) {
            picker.clearFocus();
            hour = picker.getCurrentHour();
            min = picker.getCurrentMinute();

            setSummary(getSummary());
            final long ms = Time.Interval.HOUR.X(hour) + Time.Interval.MINUTE.X(min);
            if (callChangeListener(ms)) {
                persistLong(ms);
                notifyChanged();
            }
        }
    }

    @Override
    protected Object onGetDefaultValue(TypedArray a, int index) {
        return a.getString(index);
    }

    @Override
    protected void onSetInitialValue(boolean restoreValue, Object defaultValue) {

        long ms = System.currentTimeMillis() + Time.tzOffsetMillis;
        if (restoreValue) {
            if (defaultValue == null)
                ms = getPersistedLong(ms);
            else
                ms = Long.parseLong(getPersistedString((String) defaultValue));
        } else {
            if (defaultValue != null)
                ms = Long.parseLong((String) defaultValue);
        }

        ms %= Time.Interval.DAY.ms;
        ms -= ms % Time.Interval.MINUTE.ms;

        hour = (int)(ms / Time.Interval.HOUR.ms) % 24;
        min = (int)((ms % Time.Interval.HOUR.ms) / Time.Interval.MINUTE.ms) % 60;

        setSummary(getSummary());
    }

    @Override
    public CharSequence getSummary() {
        return (hour < 10 ? "0" : "") + hour + ":" + (min < 10 ? "0" : "") + min;
    }
}
