package org.bitreich.wecker.view.dialog;

import android.app.Activity;
import android.app.KeyguardManager;
import android.app.KeyguardManager.KeyguardLock;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import org.bitreich.wecker.R;
import org.bitreich.wecker.WeckerAction;
import org.bitreich.wecker.service.WSHelper;
import org.bitreich.wecker.util.Prefs;
import org.bitreich.wecker.util.Time;
import org.bitreich.wecker.util.Time.Interval;
import org.bitreich.wecker.util.VibratePattern;
import org.bitreich.wecker.util.WeckerVo;

public class WeckerDismissDialog extends Activity implements OnClickListener, OnLongClickListener {

    final static protected int WINDOW_FLAGS = 0
            | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
            | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
            | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
            | WindowManager.LayoutParams.FLAG_FULLSCREEN
            | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
            ;

    protected BroadcastReceiver mDismissReceiver = null;
    protected BroadcastReceiver mScreenOffReceiver = null;
    protected Vibrator mVibrator = null;
    protected boolean mHasConfigurationChanged = false;
    protected WeckerVo mWeckerVo;
    protected Handler mHandler;
    protected Runnable mUpdateTimeRunnable;

    private KeyguardLock mScreenLock;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            mWeckerVo = WeckerVo.fromIntent(getIntent());
        } catch (Exception ex) {
            Log.e(WeckerAction.NS, "Fail to parse intent.");
            finish();
            return;
        }

        getWindow().addFlags(WINDOW_FLAGS);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB_MR2)
            mScreenLock = ((KeyguardManager) getSystemService(Context.KEYGUARD_SERVICE))
                    .newKeyguardLock("WeckerScreenLock");

        Time.init(this);

        final Typeface font = Typeface.createFromAsset(
                getAssets(),
                "fonts/roboto-thin.ttf");

        setContentView(R.layout.dismiss_dialog);

        mVibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

        final Button btn = (Button) findViewById(R.id.dismiss_button);
        btn.setTypeface(font);
        btn.setOnClickListener(this);
        btn.setOnLongClickListener(this);
        updateDismissTime();

        registerReceiver(mDismissReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(final Context context, final Intent intent) {
                try {
                    final WeckerVo w = WeckerVo.fromIntent(intent);
                    if (w.time == mWeckerVo.time) {
                        mWeckerVo = null;
                        if (!isFinishing())
                            finish();
                    }
                } catch (final Exception ex) {}
            }
        }, new IntentFilter(WeckerAction.DISMISSED));

        registerReceiver(mScreenOffReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(final Context context, final Intent intent) {
                sendAndFinish(WeckerAction.DISMISS);
            }
        }, new IntentFilter(Intent.ACTION_SCREEN_OFF));

        mHandler = new Handler();
        mUpdateTimeRunnable = new Runnable() {

            @Override
            public void run() {
                updateDismissTime();
                scheduleTimeUpdate();
            }
        };
        scheduleTimeUpdate();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mScreenLock != null)
            mScreenLock.disableKeyguard();
    }

    @Override
    protected void onPause() {
        super.onPause();

		try {
            unregisterReceiver(mDismissReceiver);
        } catch (final Exception e) {}

        try {
            unregisterReceiver(mScreenOffReceiver);
        } catch (final Exception e) {}

        try {
            mHandler.removeCallbacks(mUpdateTimeRunnable);
        } catch (final Exception e) {}
    }

    protected void updateDismissTime() {
        try {
            final Button btn = (Button) findViewById(R.id.dismiss_button);
            btn.setText( Time.print((int)(Time.localTodayMillis() / Time.Interval.SECOND.ms)) );
        } catch (final Exception ex) {}
    }

    protected void scheduleTimeUpdate() {
        if (mUpdateTimeRunnable != null)
            mHandler.postDelayed(mUpdateTimeRunnable, Time.millisToNext(Interval.MINUTE));
    }

    @Override
    public void onClick(final View paramView) {
        sendAndFinish(WeckerAction.SNOOZE);
    }

    @Override
    public boolean onLongClick(final View paramView) {
        sendAndFinish(WeckerAction.DISMISS);
        return true;
    }

    @Override
    public boolean onKeyLongPress(final int keyCode, final KeyEvent event) {
        if (skipKeyCode(keyCode))
            return false;
        sendAndFinish(WeckerAction.DISMISS);
        return keyCode != KeyEvent.KEYCODE_POWER;
    }

    @Override
    public boolean onKeyDown(final int keyCode, final KeyEvent event) {
        if (skipKeyCode(keyCode))
            return false;

        final boolean result;
        switch (keyCode) {
        case KeyEvent.KEYCODE_POWER:
        case KeyEvent.KEYCODE_BACK:
        case KeyEvent.KEYCODE_HOME:
        case KeyEvent.KEYCODE_CALL:
        case KeyEvent.KEYCODE_ENDCALL:
        case KeyEvent.KEYCODE_MENU:
            result = false;
            break;
        default:
            result = true;
        }

        sendAndFinish(WeckerAction.DISMISS);
        return result;
    }

    @Override
    protected void onUserLeaveHint() {
        super.onUserLeaveHint();
        sendAndFinish(WeckerAction.DISMISS);
    }

    static protected boolean skipKeyCode(final int keyCode) {
        switch (keyCode) {
        case KeyEvent.KEYCODE_DPAD_CENTER:
        case KeyEvent.KEYCODE_DPAD_UP:
        case KeyEvent.KEYCODE_DPAD_RIGHT:
        case KeyEvent.KEYCODE_DPAD_DOWN:
        case KeyEvent.KEYCODE_DPAD_LEFT:
            return true;
        default:
            return false;
        }
    }

    @Override
    public void onConfigurationChanged(final Configuration newConfig) {
        mHasConfigurationChanged = getResources().getConfiguration().compareTo(newConfig) != 0;
        super.onConfigurationChanged(newConfig);
    }

    protected void sendAndFinish(final String action) {
        if (mWeckerVo == null)
            return;

        final Intent i = WSHelper.newWeckerIntent(this, mWeckerVo.time, mWeckerVo.type.ordinal(), action);
        mWeckerVo = null;

        mVibrator.cancel();
        final boolean isSnooze = WeckerAction.SNOOZE.equals(action);
        if (isSnooze) {
            Toast.makeText(this,
                    String.format(
                            getString(R.string.snooze_mins),
                            (Prefs.getTimeout(this, Prefs.KEY.TIMEOUT_SNOOZE) / Time.Interval.MINUTE.ms)),
                    Toast.LENGTH_LONG).show();
            mVibrator.vibrate(VibratePattern.ON_SNOOZE, -1);
        } else
            mVibrator.vibrate(VibratePattern.ON_DISMISS, -1);

        startService(i);

        if (!isFinishing())
            finish();
    }

    @Override
    protected void onDestroy() {
		if (!mHasConfigurationChanged)
			sendAndFinish(WeckerAction.SNOOZE);

        super.onDestroy();

		if (mScreenLock != null)
			mScreenLock.reenableKeyguard();
    }
}
