package org.bitreich.wecker.view.dialog;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;

import org.bitreich.wecker.R;
import org.bitreich.wecker.WeckerAction;
import org.bitreich.wecker.data.DataDescriptor.WeckerType;
import org.bitreich.wecker.util.Time;
import org.bitreich.wecker.view.picker.TimePicker;

import java.util.Calendar;

public class TimePickerDialog extends DialogFragment {
	public static final String ID = WeckerAction.NS + ".TimePickerDialog";

	protected OnTimeSetListener mOnTimeSetCallback;
	protected WeckerType mWeckerType;

	public interface OnTimeSetListener {
		void onTimeSetListener(final WeckerType type, int hourOfDay, int minute);
	}

	public static TimePickerDialog newInstance(final OnTimeSetListener callback, final WeckerType type) {
		final TimePickerDialog d = new TimePickerDialog();
		d.mOnTimeSetCallback = callback;
		d.mWeckerType = type;
		return d;
	}

	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		final int H, M;
		final boolean is24H;
		if (mWeckerType == WeckerType.ALARM) {
			final Calendar calendar = Calendar.getInstance();
			calendar.add(Calendar.MINUTE, 1);
			H = calendar.get(Calendar.HOUR_OF_DAY);
			M = calendar.get(Calendar.MINUTE);
			is24H = Time.is24h;
		} else {
			H = 0;
			M = 15;
			is24H = true;
		}

		setStyle(DialogFragment.STYLE_NO_TITLE, 0);

	}

	@Override
	public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
		final View v = inflater.inflate(R.layout.timepicker_dialog, null);

		final Dialog dialog = getDialog();
		dialog.setCancelable(true);
		dialog.setCanceledOnTouchOutside(true);

		v.findViewById(R.id.cancel_button).setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(final View v) {
				dismiss();
			}
		});

		final TimePicker p = (TimePicker) v.findViewById(R.id.time_picker);
		p.setType(mWeckerType);
		final View setButton = v.findViewById(R.id.set_button);
		p.setSetButton(setButton);
		setButton.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(final View view) {
				if (mOnTimeSetCallback != null) {
					mOnTimeSetCallback.onTimeSetListener(mWeckerType, p.getCurrentHour(), p.getCurrentMinute());
					dismiss();
				}
			}
		});

		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		return v;
	}

	@Override
	public void onStart() {
		super.onStart();

		if (getDialog() != null)
			getDialog().getWindow().setLayout(
					(int)getResources().getDimension(R.dimen.dialog_width),
					LayoutParams.WRAP_CONTENT);
	}

	@Override
	public void onStop() {
		super.onStop();
		mOnTimeSetCallback = null;
	}
}
