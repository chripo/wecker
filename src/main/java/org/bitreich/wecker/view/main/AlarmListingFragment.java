package org.bitreich.wecker.view.main;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.Point;
import android.graphics.RadialGradient;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.os.Build;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.Toast;

import org.bitreich.wecker.R;
import org.bitreich.wecker.data.DataDescriptor.Alarm;
import org.bitreich.wecker.data.DataDescriptor.WeckerState;
import org.bitreich.wecker.data.DataDescriptor.WeckerType;
import org.bitreich.wecker.receiver.NotificationReceiver;
import org.bitreich.wecker.service.WSHelper;
import org.bitreich.wecker.service.WeckerService.FLAG;
import org.bitreich.wecker.util.Screen;
import org.bitreich.wecker.util.Time;
import org.bitreich.wecker.util.VibratePattern;
import org.bitreich.wecker.util.WeckerVo;
import org.bitreich.wecker.view.AbsWeckerFragment;
import org.bitreich.wecker.view.dialog.TimePickerDialog;
import org.bitreich.wecker.widget.AlarmButton;

public class AlarmListingFragment extends AbsWeckerFragment implements OnItemLongClickListener, View.OnClickListener {

    protected GridView mGrid;

    protected WeckerType mWeckerType;

    protected Runnable mAutoCloseRunner = null;

    protected Vibrator mVibrator;

    protected int mJumpToTime = -1;

    protected DataSetObserver mDataChangedObserver;

    public AlarmListingFragment() {
    }

    public static Fragment newInstance(final WeckerType type) {
        final AlarmListingFragment f = new AlarmListingFragment();
        final Bundle args = new Bundle();
        args.putInt("type", type.ordinal());
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mWeckerType = WeckerType.values()[getArguments().getInt("type")];
        mVibrator = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        final View v = inflater.inflate(R.layout.alarmlisting, container, false);

        final Typeface font = Typeface.createFromAsset(
                                    getActivity().getAssets(),
                                    "fonts/roboto-thin.ttf");

        setBackgroundGradient( getActivity(),
                v.findViewById(R.id.fragment_view),
                mWeckerType == WeckerType.ALARM
                            ? new int[]{0xff00c7c1, 0xff00a7a1, 0xff006b67}
                            : new int[]{0xffff5a0a, 0xffeb5a0a, 0xffa13d07});

        mGrid = (GridView) v.findViewById(R.id.fragment_view_grid);
        mGrid.setOnItemLongClickListener(this);
        final AlarmListingAdapter adapter = new AlarmListingAdapter(this, getActivity(), mWeckerType, font);
        adapter.registerDataSetObserver( mDataChangedObserver = new DataSetObserver() {

            @Override
            public void onChanged() {
                if (mGrid != null) {
                    if (mJumpToTime >= 0) {
                        final int pos = adapter.getItemPositionByTime(mJumpToTime);
                        mJumpToTime = -2;
                        jmpTo(pos);

                    } else if (mJumpToTime == -1) {
                        mJumpToTime = -2;
                        mGrid.postDelayed(new Runnable(){

                            @Override
                            public void run() {
                                final int pos = adapter.getNextEnabledPosition();
                                if (pos > 0)
                                    jmpTo(pos);
                            }
                        }, 500);
                    }
                }
            }

            @TargetApi(Build.VERSION_CODES.FROYO)
            protected void jmpTo(final int pos) {
                 mGrid.postDelayed(new Runnable(){

                     @Override
                     public void run() {
                         try {
                             if (Build.VERSION.SDK_INT < Build.VERSION_CODES.FROYO)
                                 mGrid.setSelection(pos);
                             else
                                 mGrid.smoothScrollToPosition(pos);
                         } catch (Exception ex) {}
                     }
                 }, 75);
            }
        });
        mGrid.setAdapter(adapter);
        adapter.reload();
        final Button btn = (Button) v.findViewById(R.id.fragment_view_header);
        btn.setTypeface(font);
        if (mWeckerType == WeckerType.TIMER)
            btn.setText(getString(R.string.timer_name));
        btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View paramView) {
                showWeckerTimePickerDialog(AlarmListingFragment.this, mWeckerType);
            }
        });

        return v;
    }

    @SuppressWarnings("deprecation")
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    static protected void setBackgroundGradient(final Activity ctx, final View v, final int[] colors) {
        final Point p = Screen.getDim(ctx);
        final ShapeDrawable bg = new ShapeDrawable(new RectShape());
        bg.getPaint().setShader(new RadialGradient(
                        0.5f * p.x,
                        0.185f * p.y,
                        0.92f * p.y,
                        colors,
                        null,
                        Shader.TileMode.CLAMP));

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN)
            v.setBackgroundDrawable(bg);
        else
            v.setBackground(bg);
    }

    @Override
    public void onPause() {
        super.onPause();
        setupAutoCloseActivity(this, false);
    }

    @Override
    public void onResume() {
        super.onResume();

        setupItemRefreshViewTask(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            final AlarmListingAdapter ad = (AlarmListingAdapter)mGrid.getAdapter();
            ad.unregisterDataSetObserver(mDataChangedObserver);
            ad.onDestroy();
        } catch (Exception ex) {}
        mGrid = null;
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int pos, long id) {
        if (!AlarmButton.class.isInstance(view))
            return false;
        doWeckerDelete(this, ((AlarmButton)view).getTime());
        return true;
    }

    @Override
    public void onClick(View view) {
        if (!AlarmButton.class.isInstance(view))
            return;

        final AlarmButton btn = (AlarmButton) view;

        btn.setState(btn.getState() == WeckerState.ENABLED ? WeckerState.DISABLED : WeckerState.ENABLED);

        final boolean isEnabled = btn.getState() == WeckerState.ENABLED;

        final long dueUTC = isEnabled ? (mWeckerType == WeckerType.ALARM
                                                ? Time.nextWecker(btn.getTime())
                                                : Time.nextTimer(btn.getTime()))
                                      : 0;

        WSHelper.scheduleWecker(getActivity(), new WeckerVo(mWeckerType, btn.getTime()), dueUTC);

        setupAutoCloseActivity(this, isEnabled);

        if (isEnabled)
            mVibrator.vibrate(VibratePattern.SHORT, -1);

        NotificationReceiver.triggerUpdate(getActivity(), FLAG.FORCED);
    }

    static protected void showWeckerTimePickerDialog(final AlarmListingFragment frg, final WeckerType type) {
        if (frg.getActivity().getSupportFragmentManager().findFragmentByTag(TimePickerDialog.ID) == null)
            TimePickerDialog.newInstance(new TimePickerDialog.OnTimeSetListener() {

				@Override
				public void onTimeSetListener(final WeckerType type, final int hourOfDay, final int minute) {
					if (type == WeckerType.ALARM || (hourOfDay > 0 || minute > 0))
						doWeckerAdd(frg.getActivity(), frg.mJumpToTime = ((hourOfDay * 60 + minute) * 60), type);
				}

			}, type).show(frg.getActivity().getSupportFragmentManager(), TimePickerDialog.ID);
    }

    static protected void doWeckerAdd(final Context ctx, final int relativeTimeSec, final WeckerType type) {
        final ContentValues vals = new ContentValues();
        vals.put(Alarm.Cols.TIME, relativeTimeSec);
        vals.put(Alarm.Cols.TYPE, type.ordinal());
        ctx.getContentResolver().insert(Alarm.CONTENT_URI, vals);
    }

    static protected void doWeckerDelete(final AlarmListingFragment ctx, final int time) {
        WSHelper.scheduleWecker(ctx.getActivity(), new WeckerVo(ctx.mWeckerType, time), 0);
        final int count = ctx.getActivity().getContentResolver().delete(
                Alarm.CONTENT_URI,
                Alarm.Cols.TIME + " = ? AND " + Alarm.Cols.TYPE + " = ? ",
                new String[]{""+time, ""+ ctx.mWeckerType.ordinal()});

        if (count > 0) {
            Toast.makeText(ctx.getActivity(),
                    String.format(ctx.getString(R.string.delete_wecker_at), Time.print(time)),
                    Toast.LENGTH_SHORT).show();

            ctx.mVibrator.vibrate(VibratePattern.LONG, -1);
            NotificationReceiver.triggerUpdate(ctx.getActivity(), FLAG.FORCED);
        }
    }

    static protected void setupItemRefreshViewTask(final AlarmListingFragment ctx) {
        if (ctx.mGrid != null) {
            ctx.mGrid.postDelayed(new Runnable() {

                @Override
                public void run() {
                    final int count;
                    if (ctx.mGrid == null || !ctx.isVisible() || ctx.isDetached() || (count = ctx.mGrid.getChildCount()) == 0)
                        return;

                    AlarmButton v;
                    for (int i = 0; i< count; ++i)
                        if ((v = (AlarmButton) ctx.mGrid.getChildAt(i).findViewById(R.id.alarm_item_time)) != null)
                            v.setTime(v.getTime());

                    setupItemRefreshViewTask(ctx);
                }
            }, Time.millisToNext(Time.Interval.MINUTE));
        }
    }

    static protected void setupAutoCloseActivity(final AlarmListingFragment ctx, final boolean set) {
        if (ctx.mGrid == null)
            return;

        if (ctx.mAutoCloseRunner != null) {
            ctx.mGrid.removeCallbacks(ctx.mAutoCloseRunner);
            ctx.mAutoCloseRunner = null;
        }

        if (set)
            ctx.mGrid.postDelayed(ctx.mAutoCloseRunner = new Runnable() {

                @Override
                public void run() {
                    ctx.mAutoCloseRunner = null;
                    try { ctx.getActivity().finish(); } catch (Exception ex) {}
                }
            }, 1500);
    }
}
