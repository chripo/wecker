package org.bitreich.wecker.view.main;

import android.app.Activity;
import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.FilterQueryProvider;

import org.bitreich.wecker.R;
import org.bitreich.wecker.data.DataDescriptor.Alarm;
import org.bitreich.wecker.data.DataDescriptor.WeckerState;
import org.bitreich.wecker.data.DataDescriptor.WeckerType;
import org.bitreich.wecker.widget.AlarmButton;

public class AlarmListingAdapter extends CursorAdapter {

    protected ContentObserver mContentObserver;
    protected Activity ctx;
    protected LayoutInflater mInf;
    protected WeckerType type;
    protected View.OnClickListener onClickListener;
    protected Typeface font;

    public AlarmListingAdapter(final View.OnClickListener onClick, final Activity ctx, final WeckerType type, final Typeface font ) {
        super(ctx, null, false);
        this.type = type;
        this.onClickListener = onClick;
        this.ctx = ctx;
        this.font = font;

        mInf = LayoutInflater.from(ctx);

        setFilterQueryProvider(new FilterQueryProvider() {

            @Override
            public Cursor runQuery(CharSequence query) {
                return AlarmListingAdapter.this.ctx.getContentResolver().query(
                        Alarm.CONTENT_URI,
                        new String[] {
                                Alarm.Cols._ID,
                                Alarm.Cols.TIME,
                                Alarm.Cols.STATE
                        },
                        Alarm.Cols.TYPE + "=" + AlarmListingAdapter.this.type.ordinal()
                                + " AND " + Alarm.Cols.TIME + " LIKE ?",
                        new String[] { "%" + query.toString() + "%" },
                        Alarm.Cols.TIME + " ASC");
            }
        });

        this.ctx.getContentResolver().registerContentObserver(
                Alarm.CONTENT_URI,
                true,
                mContentObserver = new ContentObserver(null) {
                    @Override
                    public void onChange(boolean selfChange) {
                        AlarmListingAdapter.this.ctx.runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                reload();
                            }
                        });
                    }
                });
    }

    public void reload(){
        changeCursor(runQueryOnBackgroundThread(""));
    }

    public void onDestroy() {
        try {
            ctx.getContentResolver().unregisterContentObserver(mContentObserver);
            mContentObserver = null;
            getCursor().close();
        } catch (Exception e) {}

        setFilterQueryProvider(null);

        ctx = null;
        mInf = null;
        onClickListener = null;
    }

    @Override
    public View newView(Context ctx, Cursor cursor, ViewGroup parent) {
        final View v = mInf.inflate(R.layout.alarmlisting_item, null);
        final AlarmButton btn = (AlarmButton) v.findViewById(R.id.alarm_item_time);
        btn.setOnClickListener(onClickListener);
        btn.setTypeface(font);

        return v;
    }

    @Override
    public void bindView(View view, Context ctx, Cursor c) {
        final AlarmButton btn = (AlarmButton) view.findViewById(R.id.alarm_item_time);
        btn.setTime(c.getInt(c.getColumnIndex(Alarm.Cols.TIME)));
        btn.setState(
                c.getInt(c.getColumnIndex(Alarm.Cols.STATE)) == WeckerState.ENABLED.ordinal()
                ? WeckerState.ENABLED
                : WeckerState.DISABLED);
    }

    public int getItemPositionByTime(final int time) {
        try {
            final Cursor c = getCursor();
            c.moveToFirst();
            do {
                if (((long)c.getInt(c.getColumnIndex(Alarm.Cols.TIME))) == time)
                    return c.getPosition();
            } while(c.moveToNext());
        } catch (Exception ex) {}
        return 0;
    }

    public int getNextEnabledPosition() {
        try {
            final Cursor c = getCursor();
            final int idx = c.getColumnIndex(Alarm.Cols.STATE);
            if (!c.moveToFirst())
                return 0;
            do {
                if (c.getInt(idx) == WeckerState.ENABLED.ordinal())
                    return c.getPosition();
            } while(c.moveToNext());
        } catch (Exception ex) {}
        return 0;
    }
}
