package org.bitreich.wecker.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;

import org.bitreich.wecker.data.DataDescriptor.WeckerState;
import org.bitreich.wecker.util.Time;

public class AlarmButton extends Button {

    private int time;
    private WeckerState state;

    public AlarmButton(Context context) {
        super(context);
    }

    public AlarmButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AlarmButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public int getTime() {
        return time;
    }

    /**
     * FIXME show snooze state issue #35
     * @param time
     */
    public void setTime(final int time) {
        this.time = time;
//      setText(Time.print(time) +  " [" + Time.printRemainingTimeHuman(time) + "]");
        setText(Time.print(time));
    }

    public WeckerState getState() {
        return state;
    }

    public void setState(final WeckerState state) {
        this.state = state;
        setSelected(this.state == WeckerState.ENABLED);
    }
}
