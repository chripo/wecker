package org.bitreich.wecker.util;

import android.content.IntentFilter;

public class Intents {

	public static IntentFilter Filter(final String... args) {
		final IntentFilter intentFilter = new IntentFilter();
		for (final String action : args)
			intentFilter.addAction(action);
		return intentFilter;
	}
}
