package org.bitreich.wecker.util;

import android.annotation.TargetApi;
import android.app.Activity;
import android.graphics.Point;
import android.os.Build;
import android.util.DisplayMetrics;
import android.view.Display;

public class Screen {

    @SuppressWarnings("deprecation")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    static public Point getDim(final Activity ctx) {
        Point p = new Point();

        final Display d = ctx.getWindowManager().getDefaultDisplay();

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB_MR2) {
            p.x = d.getWidth();
            p.y = d.getHeight();
        } else {
            final DisplayMetrics metrics = new DisplayMetrics();
            d.getMetrics(metrics);
            p.x = metrics.widthPixels;
            p.y = metrics.heightPixels;
        }
        return p;
    }

}
