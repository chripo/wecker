package org.bitreich.wecker.util;

import org.bitreich.wecker.WeckerAction.Extra;
import org.bitreich.wecker.data.DataDescriptor.WeckerType;

import android.content.Intent;

public class WeckerVo {
   public int time;
   public WeckerType type;

   public WeckerVo() {}

   public WeckerVo(final WeckerType type, final int time) {
       this.type = type;
       this.time = time;
   }

   public static WeckerVo fromIntent(final Intent i) {
       return new WeckerVo(
               i.getIntExtra(Extra.WECKER_TYPE, WeckerType.TIMER.ordinal()) == WeckerType.ALARM.ordinal()
                       ? WeckerType.ALARM
                       : WeckerType.TIMER,
               i.getIntExtra(Extra.WECKER_TIME, -1));
   }
}
