package org.bitreich.wecker.util;

import org.bitreich.wecker.WeckerAction;

import android.app.AlarmManager;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Build;
import android.provider.Settings;
import android.util.Log;

public class ContextHelper {

     public static AlarmManager getAlarmManager(final Context ctx) {
         return (AlarmManager) ctx.getSystemService(Context.ALARM_SERVICE);
     }

     public static void setMute(final Context c, final boolean isMute) {
         final AudioManager am = (AudioManager) c.getSystemService(Context.AUDIO_SERVICE);

         if (am.getRingerMode() == AudioManager.RINGER_MODE_SILENT && !isMute)
             am.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
         else
         if (am.getRingerMode() == AudioManager.RINGER_MODE_NORMAL && isMute)
             am.setRingerMode(AudioManager.RINGER_MODE_SILENT);
     }

    @SuppressWarnings("deprecation")
    public static void setAirplane(final Context c, final boolean enable) {
         if (Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN) {
             setMute(c, enable);
         } else {
             final boolean isEnabled;
             try {
                 isEnabled = Settings.System.getInt(
                           c.getContentResolver(),
                           Settings.System.AIRPLANE_MODE_ON, 0) == 1;

                 if (isEnabled != enable) {
                     Settings.System.putInt(
                               c.getContentResolver(),
                               Settings.System.AIRPLANE_MODE_ON, enable ? 1 : 0);
                 } else
                     return;
              } catch (Exception e) {
                  Log.w(WeckerAction.NS, "fail to set airplane mode: " + e.getMessage());
                  setMute(c, enable);
                  return;
              }

              Intent intent = new Intent(Intent.ACTION_AIRPLANE_MODE_CHANGED);
              intent.putExtra("state", enable);
              c.sendBroadcast(intent);
         }
    }
}
