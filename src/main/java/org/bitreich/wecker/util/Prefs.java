package org.bitreich.wecker.util;


import java.util.Locale;

import org.bitreich.wecker.WeckerAction;
import org.bitreich.wecker.util.Prefs.VALUES.AUTOSETUP;
import org.bitreich.wecker.util.Prefs.VALUES.QUIETMODE;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.preference.PreferenceManager;

public class Prefs {

    public interface KEY {
        String AUTOSETUP = "pref_autosetup";
        String LOCKSCREEN = "pref_lockscreen";
        String RINGTONE = "pref_ringtone";

        String TIMEOUT_SNOOZE = "pref_timeout_snooze";
        String TIMEOUT_DISMISS = "pref_timeout_autodismiss";

        String QUIET_MODE = "pref_quiet_mode";
        String QUIET_START = "pref_quiet_start";
    }

    public interface VALUES {
        public static enum AUTOSETUP {
            DISABLED("disabled"),
            DAILY("daily");

            final public String value;
            private AUTOSETUP(final String val) {
                value = val;
            }
        }

        public static enum QUIETMODE {
            DISABLED("disabled"),
            MUTE("mute"),
            AIRPLANE("airplane");

            final public String value;
            private QUIETMODE(final String val) {
                value = val;
            }
        }
    }

    public static enum PRIVATE {
        IS_QUIET_MODE_ACTIVE("is_quiet_mode_active");

        final public String value;
        private PRIVATE(final String val) {
            value = val;
        }
    }

    /**
     * FIXME sharedPrefs and data type
     */
    public static long getTimeout(final Context c, final String timeout_key) {
        return Long.parseLong(defaultPrefs(c).getString(timeout_key, "" + Time.Interval.MINUTE.X(7)));
    }

    public static AUTOSETUP getAutosetup(final Context c) {
        try {
            return AUTOSETUP.valueOf(
                    defaultPrefs(c).getString(KEY.AUTOSETUP, AUTOSETUP.DISABLED.value)
                        .toUpperCase(Locale.US));
        } catch (Exception e) {}
        return AUTOSETUP.DISABLED;
    }

    public static Uri getRingtone(final Context c) {
        try {
            return Uri.parse(defaultPrefs(c).getString(KEY.RINGTONE, null));
        } catch ( Exception e ) {}
        return RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
    }

    public static QUIETMODE getQuietMode(final Context c) {
        try {
            return QUIETMODE.valueOf(
                    defaultPrefs(c).getString(KEY.QUIET_MODE, QUIETMODE.DISABLED.value)
                        .toUpperCase(Locale.US));
        } catch (Exception e) {}
        return QUIETMODE.DISABLED;
    }

    public static long getQuietStart(final Context c) {
        long time = defaultPrefs(c).getLong(KEY.QUIET_START, Time.Interval.HOUR.X(23));
        time = (Time.Interval.DAY.ms + time - Time.tzOffsetMillis) % Time.Interval.DAY.ms;
        time -= time % Time.Interval.MINUTE.ms;
        return time;
    }

    static public boolean showOnLockscreen(final Context c) {
        return defaultPrefs(c).getBoolean(KEY.LOCKSCREEN, true);
    }

    protected static SharedPreferences defaultPrefs(final Context c) {
        return PreferenceManager.getDefaultSharedPreferences(c);
    }

    public static SharedPreferences prefs(final Context c) {
        return c.getSharedPreferences(WeckerAction.NS+".prefs", 0);
    }

    public static boolean setBoolIf(final Context c, final PRIVATE key, final boolean value, final boolean ifVal) {
        return setBoolIf(c, key, value, ifVal, false);
    }

    @SuppressLint("NewApi")
    public static boolean setBoolIf(final Context c, final PRIVATE key, final boolean value, final boolean ifVal, final boolean isForced) {
        final SharedPreferences p = prefs(c);
        if (isForced || p.getBoolean(key.value, ifVal) == ifVal) {
            final SharedPreferences.Editor e = p.edit();
            e.putBoolean(key.value, value);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD)
                e.apply();
            else
                e.commit();
            return true;
        }
        return false;
    }
}
