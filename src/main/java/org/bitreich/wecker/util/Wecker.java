package org.bitreich.wecker.util;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Handler;
import android.os.Vibrator;
import android.util.Log;

import org.bitreich.wecker.WeckerAction;

public enum Wecker {
    INSTANCE;

    protected MediaPlayer mPlayer = null;
    protected Ringtone mFallback = null;
    protected Uri mAlartTone = null;
    protected VolumeFadeInTask mVolTask = null;
    protected Vibrator mVibrator;

    Wecker() {
        mPlayer = new MediaPlayer();
        mVolTask = new VolumeFadeInTask(0.05f);
    }

    protected void setVolume(final float vol) {
        mPlayer.setVolume(vol, vol);
    }

    protected void init(final Context ctx) {
        if (mVibrator == null)
            mVibrator = (Vibrator) ctx.getSystemService(Context.VIBRATOR_SERVICE);
        mAlartTone = getAlertTone(ctx);
        if (mFallback == null)
            mFallback = RingtoneManager.getRingtone(ctx, mAlartTone);
        if (mFallback != null)
            mFallback.setStreamType(AudioManager.STREAM_ALARM);
    }

    static protected Uri getAlertTone(final Context ctx) {
        Uri tone = Prefs.getRingtone(ctx);
        if (tone == null) {
            tone = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            if (tone == null) {
                tone = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
                if (tone == null)
                    tone = RingtoneManager.getValidRingtoneUri(ctx);
            }
        }
        return tone;
    }

    public boolean isPlaying() {
        return mPlayer.isPlaying() || (mFallback != null && mFallback.isPlaying());
    }

    protected void ensurePlay() {
        try {
            if (!mPlayer.isPlaying() && mFallback != null && !mFallback.isPlaying())
                mFallback.play();
        } catch (Exception ex) {}
    }

    public void play(final Context ctx, final int fadeIn) {
        init(ctx);
        stop();

        try {
            mPlayer.setDataSource(ctx, mAlartTone);
            mPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
            mPlayer.prepare();
            mPlayer.setLooping(true);
            mVolTask.fadeIn(ctx, fadeIn);
            mPlayer.start();
        } catch (final Exception ex) {
            Log.e(WeckerAction.NS, "Error: " + ex.getMessage(), ex);
        }

        mVibrator.vibrate(VibratePattern.WECKER, 0);
        ensurePlay();
    }

    public void stop() {
        try {
            mVibrator.cancel();
        } catch (final Exception e) {}
        mVolTask.stop();
        if (mPlayer.isPlaying())
            mPlayer.stop();
        mPlayer.reset();
        try {
            mFallback.stop();
        } catch (final Exception e) {}
    }

    protected final class VolumeFadeInTask implements Runnable {
        protected final Handler h = new Handler();
        protected final float VOL_MIN;
        protected final float VOL_MAX;
        protected float x;
        protected float inc;

        public VolumeFadeInTask(){
            this(0f);
        }

        public VolumeFadeInTask(final float startVol) {
            this(startVol, 1f);
        }

        public VolumeFadeInTask(final float startVol, final float endVol){
            VOL_MAX = endVol < 0f || endVol > 1f ? 1f : endVol;
            VOL_MIN = startVol > endVol || startVol < 0 ? 0 : startVol;
        }

        public void fadeIn(final Context ctx, final int duration) {
            reset(ctx, duration);
            h.postDelayed(VolumeFadeInTask.this, 250);
        }

        public void stop() {
            h.removeCallbacks(VolumeFadeInTask.this);
        }

        protected void reset(final Context ctx, final int fadeInDuration) {
            h.removeCallbacks(VolumeFadeInTask.this);
            x = (float)Math.pow((double)VOL_MIN, 0.25);
            inc = (1f - x) / (fadeInDuration * 4);
            Wecker.INSTANCE.setVolume(VOL_MIN);
        }

        @Override
        public void run() {
            x += inc;

            final float v = x * x * x * x;

            if (v > VOL_MAX) {
                Wecker.INSTANCE.setVolume(VOL_MAX);
                return;
            }

            Wecker.INSTANCE.setVolume(v);
            h.postDelayed(VolumeFadeInTask.this, 250);
        }
    }
}

