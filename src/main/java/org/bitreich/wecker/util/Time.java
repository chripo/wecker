package org.bitreich.wecker.util;

import java.util.TimeZone;

import org.bitreich.wecker.R;

import android.content.Context;

public class Time {

     public enum Interval {
        SECOND(1000),
        MINUTE(60 * 1000),
        HOUR(60 * 60 * 1000),
        DAY(24 * 60 * 60 * 1000);

        public final long ms;

        Interval(long ms) {
            this.ms = ms;
        }

        public int x(int factor) {
            return (int)ms * factor;
        }
        public long X(int factor) {
            return ms * factor;
        }
    }

    static public boolean is24h = false;
    static public int tzOffsetMillis = 0;
    private static String sHour;
    private static String sHours;
    private static String sMinute;
    private static String sMinutes;
    private static String sAM;
    private static String sPM;

    public static void init(final Context ctx) {
//        is24h = DateFormat.is24HourFormat(ctx);
        is24h = true;
        tzOffsetMillis = TimeZone.getDefault().getOffset(System.currentTimeMillis());
        sHour = ctx.getString(R.string.time_hour);
        sHours = ctx.getString(R.string.time_hours);
        sMinute = ctx.getString(R.string.time_minute);
        sMinutes = ctx.getString(R.string.time_minutes);
        sAM = ctx.getString(R.string.time_am);
        sPM = ctx.getString(R.string.time_pm);
    }

    public static long now() {
        return nowTruncatedTo(Interval.MINUTE);
    }

    public static long nowTruncatedTo(final Interval interval) {
        final long now = System.currentTimeMillis();
        return now - now % interval.ms;
    }

    public static long millisToNext(final Interval interval) {
        final long now = System.currentTimeMillis();
        return interval.ms - now % interval.ms;
    }

    public static long nextTimer(final int timeSec) {
        return nowTruncatedTo(Interval.SECOND) + Interval.SECOND.X(timeSec);
    }

    public static long nextWecker(final int localRelativeTimeSec) {
        final long n = System.currentTimeMillis();
        final long msToday = n % Interval.DAY.ms;
        final long nextOff = (Interval.DAY.ms + Interval.SECOND.X(localRelativeTimeSec) - tzOffsetMillis)
                % Interval.DAY.ms - msToday;

        return n + (nextOff <= 0 ? Interval.DAY.ms : 0) + nextOff;
    }

    public static long localTodayMillis() {
        return (System.currentTimeMillis() + tzOffsetMillis) % Interval.DAY.ms;
    }

    public static long localRemainingMillis(final int relativeTimeSec) {
        final long now = localTodayMillis();
        final long ms = relativeTimeSec * Interval.SECOND.ms;
        return (ms <= now ? Interval.DAY.ms : 0) + (ms - now);
    }

    public static String print(final long millisToday) {
        return print((int)(millisToday / Interval.SECOND.ms), is24h);
    }

    public static String print(final int secondsToday) {
        return print(secondsToday, is24h);
    }

    public static String print(int secondsToday, Boolean is24h) {
        if (secondsToday >= 86400)
            secondsToday %= 86400;
        final int h = (secondsToday / 3600);
        final int m = (secondsToday % 3600) / 60;

        if (is24h)
            return (h < 10 ? "0" : "" ) + h + ":" + (m < 10 ? "0": "") + m;
        else
            return (( h == 0 || h == 12 ? 12 : 0 ) + (h % 12)) + ":" + (m < 10 ? "0": "") + m + " " + (h < 12 ? sAM : sPM);
    }

    public static String printRemainingTimeHuman(final int time) {
        return printRemainingHuman(localRemainingMillis(time));
    }

    /**
     * TODO refactor
     */
    public static String printRemainingHuman(final long millis) {
        final long sec = millis / 1000;
        final int h = (int)(sec / 3600);
        final int m = (int)(sec % 3600) / 60;

        return ( h == 0 && m == 0
                ?
                    "< " + String.format(sMinute, 1)
                :
                   ( h > 0 ? String.format(h == 1 ? sHour : sHours, h) : "" ) +
                   ( h > 0 && m > 0  ? " " : "" ) +
                   ( m > 0 ? String.format(m == 1 ? sMinute : sMinutes, m) : "")
                );
    }
}
