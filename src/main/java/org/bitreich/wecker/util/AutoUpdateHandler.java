package org.bitreich.wecker.util;

import android.os.Handler;

final public class AutoUpdateHandler extends Handler {
    protected Runnable mRunnable;
    protected boolean mIsEnabled = false;
    protected Time.Interval mUpdateInterval;

    public AutoUpdateHandler(final Runnable r, final Time.Interval interval) {
        this.mRunnable = r;
        setInterval(interval);
    }

    public void setInterval(final Time.Interval interval) {
        mUpdateInterval = interval;
    }

    public void ensureStarted() {
        if (!mIsEnabled && mRunnable != null) {
            mIsEnabled = true;
            start();
        }
    }

    public void start() {
        if (mIsEnabled)
            postDelayed(mRunnable, Time.millisToNext(mUpdateInterval));
    }

    public void stop() {
        mIsEnabled = false;
        removeCallbacks(mRunnable);
    }

    public void destroy() {
        stop();
        mRunnable = null;
    }
}

