package org.bitreich.wecker.util;

public interface VibratePattern {

    public static final long[] WECKER = new long[] {2500, 500, 2500};

    public static final long[] ON_SNOOZE = new long[] {500, 50};

    public static final long[] ON_DISMISS = new long[] {500, 50, 50, 50};

    public static final long[] SHORT = new long[] {0, 50};

    public static final long[] LONG = new long[] {0, 50, 50, 50};
}
