package org.bitreich.wecker.service;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.util.Log;

import org.bitreich.wecker.WeckerAction;
import org.bitreich.wecker.data.DataDescriptor.Alarm;
import org.bitreich.wecker.data.DataDescriptor.WeckerState;
import org.bitreich.wecker.data.DataDescriptor.WeckerType;
import org.bitreich.wecker.util.AutoUpdateHandler;
import org.bitreich.wecker.util.Intents;
import org.bitreich.wecker.util.Prefs;
import org.bitreich.wecker.util.Prefs.VALUES.AUTOSETUP;
import org.bitreich.wecker.util.Time;
import org.bitreich.wecker.util.Time.Interval;
import org.bitreich.wecker.util.Wecker;
import org.bitreich.wecker.util.WeckerVo;

public class WeckerService extends Service {

    public static final String EXTRA_FLAG = "EXTRA_FLAG";
    static public enum FLAG { NONE, FORCED }

    protected long mNextDue = -1;
    protected boolean isWeckerRunning = false;

    protected AutoUpdateHandler mAutoUpdater;
    protected Runnable mShutdownRunnable;
    protected BroadcastReceiver mScreenOnRceiver;
    protected boolean isScreenOn = false;
    private WakeLock mWakeLock;

    @Override
    public void onCreate() {
        super.onCreate();

        Time.init(this);

        mAutoUpdater = new AutoUpdateHandler(new Runnable() {

            @Override
            public void run() {
                handleIntent(null, 0);
                mAutoUpdater.start();
            }
        }, Interval.MINUTE);

        mShutdownRunnable = new Runnable() {

            @Override
            public void run() {
                if (mNextDue < System.currentTimeMillis()) {
                    Log.v(WeckerAction.NS, "WeckerService: shutdown");
                    mAutoUpdater.stop();
                    stopSelf();
                }
            }
        };

        mScreenOnRceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(final Context context, final Intent intent) {
                isScreenOn = Intent.ACTION_SCREEN_ON.equals(intent.getAction());

                mAutoUpdater.stop();
                if (isScreenOn)
                    mAutoUpdater.ensureStarted();

                handleIntent(null, 0);
            }
        };

        registerReceiver(mScreenOnRceiver, Intents.Filter(
                Intent.ACTION_SCREEN_ON,
                Intent.ACTION_SCREEN_OFF));
    }

    protected void handleIntent(final Intent intent, final int startId) {

        mAutoUpdater.removeCallbacks(mShutdownRunnable);

        if (intent != null) {
            final String action = intent.getAction();
            Log.v(WeckerAction.NS, "WeckerService on " + action);

            if (WeckerAction.NOTIFICATION.equals(action)) {
                final boolean isForced = intent.getIntExtra(EXTRA_FLAG, FLAG.NONE.ordinal()) == FLAG.FORCED.ordinal();
                if (isForced)
                    mAutoUpdater.ensureStarted();
                updateNotifiation(isForced);
                if (isForced)
                    WSHelper.setupQuietMode(this, mNextDue, false);


            } else if (WeckerAction.RESTORE.equals(action)) {
                final long nDue = restoreWeckers(this);
                if (nDue > 0 && nDue < mNextDue)
                    mNextDue = nDue;
                if (!isWeckerRunning) {
                    updateNotifiation(false);
                    WSHelper.setupQuietMode(this, mNextDue, false);
                }


            } else if (WeckerAction.QUIETMODE.equals(action)) {
                WSHelper.setupQuietMode(this, mNextDue, false);


            } else {
                final WeckerVo w = WeckerVo.fromIntent(intent);

                if (WeckerAction.WECKER.equals(action)) {
                    if (isWeckerRunning)
                        return;

                    WSHelper.setupAutoDismissWeckerIntent(this, w, true);
                    weckerStart();
                    WSHelper.startDismissActivity(this, w);


                } else if (isWeckerRunning) {

                    if (WeckerAction.DISMISS.equals(action)) {
                        weckerStop();
                        WSHelper.setupAutoDismissWeckerIntent(this, w, false);
                        WSHelper.updateDatabase(this, w,
                                WSHelper.newWeckerContentValues(WeckerState.DISABLED, 0));
                        updateNotifiation(true);
                        WSHelper.setupQuietMode(this, mNextDue, true);
                        WSHelper.broadcastWeckerDissmissed(this, w);
                        WSHelper.autosetupWecker(this, w);


                    } else if (WeckerAction.SNOOZE.equals(action)) {
                        weckerStop();
                        WSHelper.setupAutoDismissWeckerIntent(this, w, false);
                        mNextDue = WSHelper.scheduleSnooze(this, w, true);
                        updateNotifiation(false);
                    }
                }
            }
        } else {
            updateNotifiation(false);
        }
    }

    protected void updateNotifiation(final boolean forced) {
        final long now = Time.now();
        if (forced || mNextDue < now) {
            mNextDue = WSHelper.getNextWeckerDue(this, now);
            if (mNextDue < 0) {
                removeNotification();
                return;
            }
            WSHelper.mLastNotificationUpdate = 0L;
        }

        if (forced || !isWeckerRunning)
            WSHelper.updateNotification(this, mNextDue, now, isScreenOn);
    }

    protected static long restoreWeckers(final Context ctx) {
        final Cursor c = WSHelper.getEnabledWeckers(ctx, 0, 0);
        if (c == null || c.getCount() == 0 || !c.moveToFirst()) {
            try { c.close(); } catch (Exception e) {}
            Log.i(WeckerAction.NS, "nothing to restore. done.");
            return -1;
        }

        final boolean isAutosetup = Prefs.getAutosetup(ctx) != AUTOSETUP.DISABLED;
        final int timeIdx = c.getColumnIndex(Alarm.Cols.TIME);
        final int typeIdx = c.getColumnIndex(Alarm.Cols.TYPE);
        final int dueIdx = c.getColumnIndex(Alarm.Cols.DUE);

        long nextDue = -1;

        do {
            final long due = c.getLong(dueIdx);
            WeckerVo w = new WeckerVo(WeckerType.values()[c.getInt(typeIdx)], c.getInt(timeIdx));

            if (due <= System.currentTimeMillis()) {
                if (isAutosetup && w.type == WeckerType.ALARM) {
                    WSHelper.autosetupWecker(ctx, w);
                } else {
                    WSHelper.updateDatabase(ctx, w,
                            WSHelper.newWeckerContentValues(WeckerState.DISABLED, 0));
                    Log.i(WeckerAction.NS, "disable " + w.type + " @ " + Time.print(w.time));
                    continue;
                }
            } else {
                WSHelper.scheduleWecker(ctx, w, due);
            }

            if (nextDue == -1)
                nextDue = due;
        } while (c.moveToNext());

        c.close();

        Log.i(WeckerAction.NS, "next wecker @ " + nextDue);

        return nextDue;
    }

    protected void removeNotification() {
        WSHelper.removeNotification(this);
        if (!isWeckerRunning) {
            mAutoUpdater.stop();
            Log.v(WeckerAction.NS, "schedule shutdown");
            mAutoUpdater.postDelayed(mShutdownRunnable, Interval.MINUTE.X(2));
        }
    }

    protected void weckerStart() {
        isWeckerRunning = true;

        if (mWakeLock == null) {
            mWakeLock = ((PowerManager)getSystemService(Context.POWER_SERVICE)).newWakeLock(
                    PowerManager.SCREEN_BRIGHT_WAKE_LOCK
                    | PowerManager.FULL_WAKE_LOCK
                    | PowerManager.ACQUIRE_CAUSES_WAKEUP,
                    "WeckerWakeLock");
            mWakeLock.setReferenceCounted(false);
        }
        if (mWakeLock.isHeld())
            mWakeLock.release();

        mWakeLock.acquire();

        Wecker.INSTANCE.play(this, 30);
    }

    protected void weckerStop() {
        isWeckerRunning = false;

        releaseLock();

        Wecker.INSTANCE.stop();
    }

    void releaseLock() {
        if (mWakeLock != null) {
            mWakeLock.release();
            mWakeLock = null;
        }
    }

    @Override
    public void onStart(Intent intent, int startId) {
        handleIntent(intent, startId);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        handleIntent(intent, startId);
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        unregisterReceiver(mScreenOnRceiver);
        mAutoUpdater.removeCallbacks(mShutdownRunnable);
        mAutoUpdater.destroy();

        releaseLock();
    }

    @Override
    public IBinder onBind(final Intent intent) {
        return null;
    }
}
