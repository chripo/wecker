package org.bitreich.wecker.service;

import android.app.AlarmManager;
import android.app.KeyguardManager;
import android.app.KeyguardManager.KeyguardLock;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import org.bitreich.wecker.MainActivity;
import org.bitreich.wecker.R;
import org.bitreich.wecker.WeckerAction;
import org.bitreich.wecker.WeckerAction.Extra;
import org.bitreich.wecker.data.DataDescriptor.Alarm;
import org.bitreich.wecker.data.DataDescriptor.WeckerState;
import org.bitreich.wecker.data.DataDescriptor.WeckerType;
import org.bitreich.wecker.service.WeckerService.FLAG;
import org.bitreich.wecker.util.ContextHelper;
import org.bitreich.wecker.util.Prefs;
import org.bitreich.wecker.util.Prefs.PRIVATE;
import org.bitreich.wecker.util.Prefs.VALUES.AUTOSETUP;
import org.bitreich.wecker.util.Prefs.VALUES.QUIETMODE;
import org.bitreich.wecker.util.Time;
import org.bitreich.wecker.util.Time.Interval;
import org.bitreich.wecker.util.WeckerVo;
import org.bitreich.wecker.view.dialog.WeckerDismissDialog;

import java.text.SimpleDateFormat;
import java.util.Locale;

public abstract class WSHelper {

    static long mLastNotificationUpdate = 0L;
    @SuppressWarnings("deprecation")
    static void updateNotification(final Context ctx, final long due, final long now, final boolean isScreenOn) {
        if (now - mLastNotificationUpdate < Interval.SECOND.X(58))
            return;
        mLastNotificationUpdate = now;

        final String remain = Time.printRemainingHuman(due - now);
        final String time = Time.print((int) (((Interval.DAY.ms + due + Time.tzOffsetMillis) % Interval.DAY.ms) / Interval.SECOND.ms));

        if (Prefs.showOnLockscreen(ctx)) {
            final KeyguardLock lock;
            if (isScreenOn) {
                final KeyguardManager kgm = (KeyguardManager) ctx.getSystemService(Context.KEYGUARD_SERVICE);
                if (kgm.inKeyguardRestrictedInputMode()) {
                    lock = kgm.newKeyguardLock("WeckerLock");
                    lock.disableKeyguard();
                } else
                    lock = null;
            } else
                lock = null;

            Settings.System.putString(ctx.getContentResolver(), Settings.System.NEXT_ALARM_FORMATTED, remain + " @ " + time);
            Log.i(WeckerAction.NS, "update notification: " + remain + " @ " + time);

            try { lock.reenableKeyguard(); } catch (Exception e) {}
        }

        final NotificationCompat.Builder nmb = new NotificationCompat.Builder(ctx)
                .setSmallIcon(R.drawable.ic_notification)
                .setContentTitle(ctx.getText(R.string.app_name) + " @ " + time)
                .setContentText(remain)
                .setAutoCancel(false)
                .setUsesChronometer(false)
                .setOngoing(true);
        final Intent intent = new Intent(ctx, MainActivity.class);
        final PendingIntent pi = PendingIntent.getActivity(ctx, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        nmb.setContentIntent(pi);
        final NotificationManager nm = (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);
        nm.notify(0, nmb.build());
    }

    static public void removeNotification(final Context ctx) {
        mLastNotificationUpdate = 0L;
        ((NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE)).cancel(0);
        Settings.System.putString(ctx.getContentResolver(), Settings.System.NEXT_ALARM_FORMATTED, "");
    }

    static public Cursor getEnabledWeckers(final Context ctx, final long now, final int limit) {
        return ctx.getContentResolver().query(
                Alarm.CONTENT_URI,
                new String[] {
                        Alarm.Cols._ID,
                        Alarm.Cols.TIME,
                        Alarm.Cols.TYPE,
                        Alarm.Cols.DUE
                },
                Alarm.Cols.STATE + " = ? AND " + Alarm.Cols.DUE + " > ?",
                new String[] { "" + WeckerState.ENABLED.ordinal(), ""+ now },
                Alarm.Cols.DUE + " ASC " + (limit > 0 ? " LIMIT " + limit : ""));
    }

    static void startDismissActivity(final Context ctx, final WeckerVo w) {
        final Intent i = new Intent(ctx, WeckerDismissDialog.class);
        putWeckerExtra(i, w.time, w.type.ordinal());
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_SINGLE_TOP);
        ctx.startActivity(i);
    }

    static void broadcastWeckerDissmissed(final Context ctx, final WeckerVo w) {
        final Intent i = new Intent(WeckerAction.DISMISSED);
        putWeckerExtra(i, w.time, w.type.ordinal());
        ctx.sendBroadcast(i);
    }

    static void setupAutoDismissWeckerIntent(final Context ctx, final WeckerVo w, final boolean set) {
        final Intent i = newWeckerIntent(ctx, w.time, w.type.ordinal(), WeckerAction.DISMISS);
        i.putExtra("autodismiss", true);
        final PendingIntent pi = PendingIntent.getService(ctx, -1, i, 0);

        final AlarmManager am = ContextHelper.getAlarmManager(ctx);
        am.cancel(pi);

        if (set) {
            final long timeout = System.currentTimeMillis()
                    + Math.max(Prefs.getTimeout(ctx, Prefs.KEY.TIMEOUT_DISMISS), Interval.MINUTE.X(2));
            am.set(AlarmManager.RTC_WAKEUP, timeout, pi);
        }
    }

    static public void scheduleWecker(final Context ctx, final WeckerVo w, final long dueUTC) {
        final Intent i = newWeckerIntent(ctx, w.time, w.type.ordinal(), WeckerAction.WECKER);
        final PendingIntent pi = PendingIntent.getService(ctx, w.time, i, PendingIntent.FLAG_CANCEL_CURRENT);
        final AlarmManager am = ContextHelper.getAlarmManager(ctx);
        am.cancel(pi);

        updateDatabase(ctx, w,
                newWeckerContentValues((dueUTC > 0 ? WeckerState.ENABLED : WeckerState.DISABLED), dueUTC));

        if (dueUTC > 0) {
            am.set(AlarmManager.RTC_WAKEUP, dueUTC, pi);

            final String scheduleTxt =
                    w.type == WeckerType.ALARM
                    ? String.format(ctx.getString(R.string.schedule_wecker_at),
                            ""+w.type,
                            Time.print(w.time),
                            Time.printRemainingTimeHuman(w.time))
                    : String.format(ctx.getString(R.string.schedule_wecker_at),
                            ""+w.type,
                            Time.print((dueUTC + Time.tzOffsetMillis) % Interval.DAY.ms),
                            Time.printRemainingHuman(w.time * Interval.SECOND.ms));
           Log.i(WeckerAction.NS, scheduleTxt + " UTC: " + dueUTC);
           Toast.makeText(ctx, scheduleTxt , Toast.LENGTH_LONG).show();
        }
    }

    static long scheduleSnooze(final Context ctx, final WeckerVo w, final boolean set) {
        final long due = set ? Time.now()
                + Math.max(Prefs.getTimeout(ctx, Prefs.KEY.TIMEOUT_SNOOZE), Interval.MINUTE.X(2)) : 0;
        scheduleWecker(ctx, w, due);
        return due;
    }

    static protected int updateDatabase(final Context ctx, final WeckerVo w, final ContentValues vals) {
       return ctx.getContentResolver().update(
               Alarm.CONTENT_URI,
               vals,
               Alarm.Cols.TIME + " = ? AND " + Alarm.Cols.TYPE + " = ? ",
               new String[]{""+w.time, ""+ w.type.ordinal()});
    }

    static protected ContentValues newWeckerContentValues(final WeckerState state, final long due) {
       final ContentValues vals = new ContentValues();
       vals.put(Alarm.Cols.STATE, state.ordinal());
       if (due > 0)
           vals.put(Alarm.Cols.DUE, due);

       return vals;
    }

    static public Intent newWeckerIntent(final Context ctx, final int time, final int type, final String action) {
       final Intent i = new Intent(ctx, WeckerService.class);
       i.setAction(action);
       putWeckerExtra(i, time, type);
       i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
       return i;
    }

    static void putWeckerExtra(final Intent i, final int time, final int type) {
        i.putExtra(Extra.WECKER_TIME, time);
        i.putExtra(Extra.WECKER_TYPE, type);
    }

    static public Intent newIntent(final Context ctx, final String action, final FLAG flag) {
       final Intent i = new Intent(ctx, WeckerService.class);
       i.setAction(action);
       i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
       i.putExtra(WeckerService.EXTRA_FLAG, flag.ordinal());

       return i;
    }

    static void setupQuietMode(final Context ctx, long nextDue, final boolean isTriggerdByWecker) {
        final QUIETMODE mode = Prefs.getQuietMode(ctx);
        if (mode != QUIETMODE.DISABLED) {
            final long quietTimeStart = Prefs.getQuietStart(ctx);
            final long quietTimeEnd = ((Interval.DAY.ms + quietTimeStart + Interval.HOUR.X(6)) % Interval.DAY.ms);
            final long now = Time.now();
            final long nowTime = now % Interval.DAY.ms;
            long nextRestModeIntent = now - nowTime + quietTimeStart;

            if (isTriggerdByWecker)
                nextDue = -1;
            else if (nextDue < 0  || nextDue < now)
                nextDue = getNextWeckerDue(ctx, now);

            if ( (quietTimeStart <= nowTime && nowTime < quietTimeEnd)
                 || ( quietTimeEnd < quietTimeStart
                         && ((quietTimeStart <= nowTime) || (nowTime < quietTimeEnd) )
                    )
                ) {
                // inbetween a quiet time
                // TODO check if due within quiet time, if not adjust and reschedule next quietModeIntent
                if (nextDue > 0)
                    activateQuietMode(ctx, mode);
            }

            if (nextDue < 0)
                resetQuietMode(ctx);

            if (nextRestModeIntent <= now)
                nextRestModeIntent += Interval.DAY.ms;

            scheduleQuietModeIntent(ctx, nextRestModeIntent);
        }
    }

    static long getNextWeckerDue(Context ctx, final long now) {
         final Cursor c = getEnabledWeckers(ctx, now, 1);
         final long due;
         if (c == null || c.getCount() == 0 || !c.moveToFirst()) {
             due = -1;
         } else
             due = c.getLong(c.getColumnIndex(Alarm.Cols.DUE));

         try { c.close(); } catch (Exception e) {}

        return due;
    }

    static protected void scheduleQuietModeIntent(final Context ctx, final long due) {
        final Intent i = newIntent(ctx, WeckerAction.QUIETMODE, FLAG.NONE);
        final PendingIntent pi = PendingIntent.getService(ctx, 0, i, PendingIntent.FLAG_CANCEL_CURRENT);
        final AlarmManager am = ContextHelper.getAlarmManager(ctx);
        am.cancel(pi);
        if (due > 0) {
            final SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mmZ", Locale.getDefault());
            Log.v(WeckerAction.NS, "schedule next QuietMode check: " + df.format(due));
            am.set(AlarmManager.RTC_WAKEUP, due, pi);
        }
    }

    static protected void activateQuietMode(final Context ctx, final QUIETMODE mode) {
        Prefs.setBoolIf(ctx, PRIVATE.IS_QUIET_MODE_ACTIVE, true, false, true);
        switch (mode) {
        case MUTE:
            ContextHelper.setMute(ctx, true);
            break;
        case AIRPLANE:
            ContextHelper.setAirplane(ctx, true);
            break;
        }
    }

    static protected void resetQuietMode(final Context ctx) {
        if (Prefs.setBoolIf(ctx, PRIVATE.IS_QUIET_MODE_ACTIVE, false, true)) {
            ContextHelper.setMute(ctx, false);
            ContextHelper.setAirplane(ctx, false);
        }
    }

    static public void autosetupWecker(final Context ctx, final WeckerVo w) {
        if (w.type != WeckerType.ALARM)
            return;

        final AUTOSETUP type = Prefs.getAutosetup(ctx);
        if (type == AUTOSETUP.DISABLED)
            return;

        scheduleWecker(ctx, w, Time.nextWecker(w.time));
    }
}
