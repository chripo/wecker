package org.bitreich.wecker;

public interface WeckerAction {
    public static final String NS = "org.bitreich.wecker";

    public static final String WECKER = NS + ".WECKER";
    public static final String SNOOZE = NS + ".SNOOZE";
    public static final String DISMISS = NS + ".DISMISS";
    public static final String DISMISSED = NS + ".DISMISSED";

    /* internal */
    public static final String NOTIFICATION = NS + ".NOTIFICATION";
    public static final String RESTORE =  NS + ".RESTORE";
    public static final String QUIETMODE = NS + ".QUIETMODE";

    public interface Extra {
        public static final String WECKER_TIME = "WECKER_TIME";
        public static final String WECKER_TYPE = "WECKER_TYPE";
    }
}
