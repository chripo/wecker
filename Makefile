
SRC := src/icons
RES := src/main/res/drawable

ICONS := $(wildcard $(SRC)/*.svg)

PNGS := $(ICONS:$(SRC)/%.svg=$(RES)-mdpi/%.png) \
		$(ICONS:$(SRC)/%.svg=$(RES)-hdpi/%.png) \
		$(ICONS:$(SRC)/%.svg=$(RES)-xhdpi/%.png) \
		$(ICONS:$(SRC)/%.svg=$(RES)-xxhdpi/%.png) \
		$(ICONS:$(SRC)/%.svg=$(RES)-xxxhdpi/%.png)

all: icons

icons: $(PNGS)

clean:
	@rm -f $(PNGS)

# MDPI
$(RES)-mdpi/ic_notification.png: $(SRC)/%.svg
	@inkscape -z -w 24 -h 24 -f $< -e $@

$(RES)-mdpi/ic_launcher.png: $(SRC)/%.svg
	@inkscape -z -w 48 -h 48 -f $< -e $@

$(RES)-mdpi/%.png: $(SRC)/%.svg
	@inkscape -z -w 32 -h 32 -f $< -e $@


# HPDI
$(RES)-hdpi/ic_notification.png: $(SRC)/%.svg
	@inkscape -z -w 36 -h 36 -f $< -e $@

$(RES)-hdpi/ic_launcher.png: $(SRC)/%.svg
	@inkscape -z -w 72 -h 72 -f $< -e $@

$(RES)-hdpi/%.png: $(SRC)/%.svg
	@inkscape -z -w 48 -h 48 -f $< -e $@


# XHDPI
$(RES)-xhdpi/ic_notification.png: $(SRC)/%.svg
	@inkscape -z -w 48 -h 48 -f $< -e $@

$(RES)-xhdpi/ic_launcher.png: $(SRC)/%.svg
	@inkscape -z -w 96 -h 96 -f $< -e $@

$(RES)-xhdpi/%.png: $(SRC)/%.svg
	@inkscape -z -w 64 -h 64 -f $< -e $@


# XXHDPI
$(RES)-xxhdpi/ic_notification.png: $(SRC)/%.svg
	@inkscape -z -w 72 -h 72 -f $< -e $@

$(RES)-xxhdpi/ic_launcher.png: $(SRC)/%.svg
	@inkscape -z -w 144 -h 144 -f $< -e $@

$(RES)-xxhdpi/%.png: $(SRC)/%.svg
	@inkscape -z -w 96 -h 96 -f $< -e $@


# XXHDPI
$(RES)-xxxhdpi/ic_notification.png: $(SRC)/%.svg
	@inkscape -z -w 96 -h 96 -f $< -e $@

$(RES)-xxxhdpi/ic_launcher.png: $(SRC)/%.svg
	@inkscape -z -w 192 -h 192 -f $< -e $@

$(RES)-xxxhdpi/%.png: $(SRC)/%.svg
	@inkscape -z -w 128 -h 128 -f $< -e $@


.PHONY: all icons clean

